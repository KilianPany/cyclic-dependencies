package at.hakwt.swp4.cyclicdeps;

public class Car {

    private Garage preferedGarage;

    public Car(Garage preferedGarage) {
        this.preferedGarage = preferedGarage;
    }

    public void move() {
        // move car
    }

}
